List of public bugs found by CKI
--------------------------------

How does this work? If a test finds a bug and there is a public tracker/email,
add a new entry under the name of the test that found it. This info includes
(but is not limited to) bug trackers, links to email threads, links to upstream
commits after the bug is resolved and kernel versions.

The main idea of this tracker is to put found bugs into a single place, as they
were distributed across different (mainly internal) places and it was hard to
find a place to link to. Note that the proper way to track new bugs may change
in the future (the initial discussion will happen at Plumbers 2019) and this
repo may end up being automatically generated or abandoned. However for
accountability purposes, let's go ahead with it for now.


Compile bugs
------------

* ppc64le compilation failing on 4.19.44-rc1 stable kernels
  * See [email thread](https://lore.kernel.org/stable/20190514201407.GA13424@archlinux-i9/)
    for details
  * Not present in mainline, commit `42e2acde1237` was backported into 4.19.44
    release to resolve the issue


Bugs found by boot testing
--------------------------

* Firmware issue with Ampere systems causing SATA errors
  * [Bug 1738660](https://bugzilla.redhat.com/show_bug.cgi?id=1738660)
  * Bug resolved with firmware version 4.9.18, workaround created for systems
    with older firware

* Firmware issue with Cavium and Gigabyte systems causing IOMMU and SATA issues
  * [Bug 1734557](https://bugzilla.redhat.com/show_bug.cgi?id=1734557)
  * Problems determined to start with kernel 5.2 (commit `954a03be033`),
    firmware needs to be updated to account for changes (kernel command line
    workarounds can be used in the meanwhile)


Bugs found by LTP
-----------------

* Hugepage migration bug found with `move_pages12` test
  * NULL pointer dereference with 4.20+ kernels
  * See [email thread](https://marc.info/?l=linux-mm&m=154646101419374&w=2) for
    details
  * Fixed with commit `ddeaab32a89` in 5.0-rc2
* Userspace getting stuck on 4.20+ kernels on aarch64 with `mtest06` test
  * See [email thread](https://lore.kernel.org/linux-mm/1817839533.20996552.1557065445233.JavaMail.zimbra@redhat.com/T/#u)
    for details
  * Fixed with commit `7a30df49f63ad` in 5.2-rc5
* Ooops hit on aarch64 with `mtest06` on 4.20+
  * See [email thread](https://lore.kernel.org/lkml/ea7ef295bc438c9d403087943c82ced56730e6e0.1563292737.git.jstancek@redhat.com/T/#u) and initial [GitHub issue](https://github.com/linux-test-project/ltp/issues/528) for details
  * Fixed with `e1b98fa31664` in 5.3-rc2
* Corrupted directory during `statx04`
  * Patch proposed [in this thread](https://lore.kernel.org/lkml/fc8878aeefea128c105c49671b2a1ac4694e1f48.1567468225.git.jstancek@redhat.com/), no resolution yet


Bugs found by blktests
----------------------

* block/001 and block/023 triggering a warning in `fs/block_dev.c`
  * Issue started with 5.3-rc3, determined to be caused by commit `89e524c04f`
  * See the [mail thread](https://marc.info/?l=linux-block&m=156524704325455&w=2)
    for details of block/001 failure (aarch64)
  * See [this Fedora bug](https://bugzilla.redhat.com/show_bug.cgi?id=1739882)
    for the details of block/023 failure (ppc64le)
  * Fixed with commit `e91455bad5` in 5.3-rc4
* NULL pointer dereference triggered by block/006 on aarch64 with 4.18 and 5.0
  kernels
  * See [mail thread](https://lore.kernel.org/linux-block/20190319042447.27580-1-ming.lei@redhat.com/T/#u)
    with v1 of the proposed fix and [this one](https://lore.kernel.org/linux-block/20190319083842.30059-1-ming.lei@redhat.com/T/#u)
    with v2
  * Fixed with `e6d1fa584e0dd` in 5.1-rc3
* block/006 triggering a page fault on 5.2-rc3 kernels
  * Seem [email thread](https://lore.kernel.org/linux-block/20190604130802.17076-1-ming.lei@redhat.com/T/)
    for details
  * Fixed with `c3e2219216` in 5.2-rc4


Bugs found by networking tests
------------------------------

* NULL pointer dereference in stable queue, fixed by removal of patches that
  caused the issue from the queue
  * See the [email thread](https://lore.kernel.org/stable/20190827183742.GA2498@kroah.com/T/#m2e5cea4db0b110b8635c9c24b8ab0484b2cd6440)
    for more details


Bugs found by KVM tests
-----------------------

* Timer test failing on 5.1-rc1 on aarch64
  * [Patch](http://lkml.iu.edu/hypermail/linux/kernel/1903.3/07339.html) posted
    and merges as commit `8fa761624871` in v5.1
* apic-split and apic tests failing TMCCT check on Xeon CPUs
  * Originally discovered on 4.18 kernels
  * Fixed with commit `3d82c565a7a2` in 5.0-rc1
* `vmware_backdoor` test failing, regression introduced in 5.1-rc6
  * No resolution yet (that I know of), [internal BZ](https://bugzilla.redhat.com/show_bug.cgi?id=1713418)
* svm test failing on AMD on 5.2-rc3 kernels
  * See [email thread](https://lore.kernel.org/kvm/20190604160939.17031-1-vkuznets@redhat.com/T/#u)
    with proposed fix for details
  * Fixed with `8f38302c0be` in 5.3-rc1
